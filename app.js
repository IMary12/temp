// Module dependencies.
var express = require("express")
  , http    = require("http")
  , path    = require("path")
  , routes  = require("./routes");
var app     = express();

var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var host = process.env.OPENSHIFT_NODEJS_IP || "localhost";

// All environments
app.set("port", port);
app.set("host", host);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, "public")));
app.use(express.errorHandler());

// App routes
app.get("/"     , routes.shop);
app.get("/shop", routes.shop);

app.get("/shop/signup", routes.signup);
app.post("/shop/signup", routes.handleSignup);

app.get("/shop/login", routes.login);
app.post("/shop/login", routes.handleLogin);

app.get("/shop/logout", routes.logout);

app.get("/shop/:category", routes.category);
app.get("/shop/:category/:subcategories", routes.subcategories);
app.get("/shop/:category/:subcategories/:products", routes.products);
app.get("/shop/:category/:subcategories/:products/:productinfo", routes.productinfo);


// Run server

http.createServer(app).listen(app.get("port"), app.get("host"), function() {
	console.log("Express server listening on " + app.get("host") + ":" + app.get("port"));
});

var _         = require("underscore");
var mdbClient = require('mongodb').MongoClient;

var mongo_db = "shop";
var mongo_host = "localhost";
var mongo_port =  27017;

var mongo_username = process.env.OPENSHIFT_MONGODB_DB_USERNAME || null;
var mongo_password = process.env.OPENSHIFT_MONGODB_DB_PASSWORD || null;

var mongo_url = createMongoURL();

var soap = require('soap');
var url = 'http://infovalutar.ro/curs.asmx?wsdl';


function createMongoURL() {
 // mongodb://username:password@host:port/db
	var url = "mongodb://";
	if (mongo_username) {
	  	url += mongo_username;
	  	if (mongo_password) {
	   	url += ":" + mongo_password;
	  	}
	  url += "@";
	}
	url += mongo_host + ":" + mongo_port + "/" + mongo_db;
	return url;
}

function findIn(coll_name, callback) {
	mdbClient.connect(mongo_url, function(err, db) {
	var collection = db.collection(coll_name);
        collection.find().toArray(function(err, items) {
            if (err) return callback(err, null);
            callback(err, items);
            db.close();
        });
    });
}

function findById(coll_name, callback, query) {
	mdbClient.connect(mongo_url, function(err, db) {
	var collection = db.collection(coll_name);
        collection.find(query).toArray(function(err, items) {
            if (err) return callback(err, null);
            callback(err, items);
            db.close();
        });
    });
}


exports.index = function(req, res) {
	res.render("index", { 
		// Template data
		title: "Express" 
	});
};

exports.shop = function(req, res) {
	findIn('categories', function(err, items) {
			res.render("shop", { 
				_     : _, 
				title : "Home",
				items : items,
				session : req.session
			});
		});
}

exports.category = function(req, res) {
	var id = req.params.category;
	findById('categories', function(err, categories) {
		console.log(categories);
		var category = _.reduce(categories, function(elem) {
			return elem.categories;
		});
		console.log(category.categories);	
		res.render("category", { 
		    _     : _, 
			title : id,
			items : categories,
			categories: category.categories,
			session : req.session
			});
		}, {"id" : id});
};


exports.subcategories = function(req, res) {
	var id = req.params.subcategories;			
	findById('categories', function(err, categories) {
		console.log(categories);
		var category = _.reduce(categories, function(elem) {			
			return elem.categories;
		});
		var subcategory = _.filter(category.categories, function(elem) {			
			return elem.id == id;
		});
		var subsubcategory = _.reduce(subcategory, function(elem) {			
			return elem.categories;
		});		
		console.log(subcategory);		
		res.render("subcategories", { 
			_     : _, 
			title : id,
			items : categories,
			subcategories: subsubcategory.categories,
			session : req.session,
			category_id: req.params.category,
			categories : subcategory
		});
	}, {"categories.id" : id});
};


exports.products = function(req, res) {
	var subcategories_id = req.params.subcategories;
	var products_id = req.params.products;
	findById('categories', function(err, categories) {
		var category = _.reduce(categories, function(elem) {			
			return elem.categories;
		});
		var subcategory = _.filter(category.categories, function(elem) {			
			return elem.id == subcategories_id;
		});
		var subsubcategory = _.reduce(subcategory, function(elem) {			
			return elem.categories;
		});		
		console.log(subcategory);	

		findById('products', function(err, products) {
			res.render("products", { 
				_     : _, 
				items : categories,
				subcategories: subsubcategory.categories,
				session : req.session,
				category_id : req.params.category,
				subcategories_id : subcategories_id,
				categories : subcategory,
				title : products_id,
				products: products
			});
		}, {"primary_category_id" : products_id});
	}, {"categories.id" : subcategories_id});
};

function FindCurr(callback) {
	soap.createClient(url, function(err, client) {
		var curr_codes = {};
		var date = new Date().toISOString();
		console.log(date);
		client.getall({dt: date}, function(err, result) {
	      	console.log(result.getallResult.diffgram.DocumentElement.Currency);
	      	_.each(result.getallResult.diffgram.DocumentElement.Currency, function(currency) {
				console.log(currency.IDMoneda +":"+currency.Value);
				curr_codes[currency.IDMoneda] = currency.Value;
			});
			curr_codes['RON'] = 1;
	      	callback(curr_codes);
	  	});
	});
}
				

exports.productinfo = function(req, res) {
	var currencies = ['USD','EUR','RON','UAH'];
	var subcategories_id = req.params.subcategories;
	var products_id = req.params.products;
	var productinfo_id = req.params.productinfo;
	findById('categories', function(err, categories) {
		var category = _.reduce(categories, function(elem) {			
			return elem.categories;
		});
		var subcategory = _.filter(category.categories, function(elem) {			
			return elem.id == subcategories_id;
		});
		var subsubcategory = _.reduce(subcategory, function(elem) {			
			return elem.categories;
		});		
		console.log(subcategory);
		findById('products', function(err, products) {
			var product = _.reduce(products, function(elem) {			
				return elem;
			});	
			
			FindCurr(function(curr_codes) {
				console.log(curr_codes);	
					
				res.render("productinfo", { 
					_     : _, 
					items : categories,
					subcategories: subsubcategory.categories,
					session : req.session,
					category_id : req.params.category,
					subcategories_id : subcategories_id,
					categories : subcategory,
					products_id : products_id,
					products: products,
					product: product,
					currencies : currencies,
					curr_codes : curr_codes
				});
			});
		}, {"id" : productinfo_id});
	}, {"categories.id" : subcategories_id});
};

function addUser(firstname, lastname, password, email, callback) {
	mdbClient.connect(mongo_url, function(err, db) {
		var users = db.collection("users");
		var user = {'_id': email, 'first-name': firstname, 'last-name':lastname, 'password': password};
		users.insert(user, function (err, result) {
   			callback(err,result);
   			db.close();    
		});	
	});
}


exports.signup = function(req, res) {
    res.render("signup", {
    	firstname : "",
    	lastname : "",
    	password : "",
        email : "",
        email_error:  "",
        session: req.session
    });
};		
      
        
exports.handleSignup = function(req, res) {
	var firstname = req.body.firstname;
	var lastname = req.body.lastname;
	var email = req.body.email;
	var password = req.body.password;
	var errors = {'email': email, session: req.session};
    addUser(firstname, lastname, password, email, function(err, user) {
        if (err) {
            if (err.code == '11000') {
                errors['email_error'] = "This email already in use. Please choose another";
                res.render("signup", errors);
            }
        } else {
        	req.session.user = user;
        	console.log("sign up: " + req.session.user);
        	res.redirect('/shop');
        }
    });
}

exports.login = function(req, res) {
    res.render("login", {
    	password : "",
        email : "",
        login_error:  "",
        session : req.session
    });
};	

function validateLogin(email, password, callback) {
	mdbClient.connect(mongo_url, function(err, db) {
	var users = db.collection("users");
    users.findOne({ '_id' : email }, function(err, user) {
	    if (err) return callback(err, null);
	    if (user) {  
	    if (password == user.password) {
                 callback(null, user);
         }   
         else {  
	        var invalid_password_error = new Error("Invalid password");
	        invalid_password_error.invalid_password = true;
	        callback(invalid_password_error, null);
	    }    
	}
	    else {
	        var no_such_user_error = new Error("User: " + user + " does not exist");
	        no_such_user_error.no_such_user = true;
	        callback(no_such_user_error, null);
	    }
	db.close();
	});	    
	});
}

exports.handleLogin = function(req, res) {
	var email = req.body.email;
	var password = req.body.password;
	validateLogin(email, password, function(err, user) {
        if (err) {
            if (err.no_such_user) {
            	console.log("No such user");
                return res.render("login", {
                	session : req.session,
                	email:email, 
                	password:"", 
                	login_error:"No such user"
                });
            }
            else if (err.invalid_password) {
            	console.log("Invalid password");
                return res.render("login", {
                	session : req.session,
                	email:email, 
                	password:"", 
                	login_error:"Invalid password"
                });
            }
        } else {
        	req.session.user = user;
        	console.log(req.session.user);
            return res.redirect('/shop');

        }
    });
}

exports.logout = function(req, res) {
	req.session.user = null;
	console.log(req.session.user);
	res.redirect("/shop");
};